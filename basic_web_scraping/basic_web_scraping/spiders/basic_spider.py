import scrapy
from scrapy.utils.reactor import install_reactor

class Title_Description_Crawler(scrapy.Spider):
    name = "course"
    install_reactor("twisted.internet.asyncioreactor.AsyncioSelectorReactor")
    
    def start_requests(self):
        start_url = "https://assets.datacamp.com/production/repositories/2560/datasets/19a0a26daa8d9db1d920b5d5607c19d6d8094b3b/all_short"
        yield scrapy.Request(url=start_url, callback=self.first_parse)

    def first_parse(self, response):
        course_blocks = response.xpath('//div[contains(@class, "course-block")]')
        course_links = course_blocks.css("a::attr(href)")
        links_to_follow = list(set(course_links.getall()))
        links_to_follow.remove('/courses/predicting-customer-churn-in-python')

        for link in links_to_follow:
            yield scrapy.Request(url=link, callback=self.second_parse)

    def second_parse(self, response):
        course = {}
        course_title = response.css("h1.header-hero__title::text").get()
        course_description = response.xpath(
            '//p[@class="course__description"]/text()'
        ).get()

        course['Title'] = course_title
        course['Description'] = course_description

        yield course