# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import psycopg2
from itemadapter import ItemAdapter

class BasicWebScrapingPipeline():
    def open_spider(self, spider):
        hostname = "localhost"
        username = "postgres"
        password = "password123"
        database = "Courses"

        self.connection = psycopg2.connect(
            host=hostname, user=username, password=password, dbname=database
        )
        self.cur = self.connection.cursor()
        self.cur.execute(
            """
            CREATE TABLE IF NOT EXISTS courses(
                title text,
                description text
            )
        """
        )

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self, item):
        self.cur.execute(
            """ insert into courses values (%s, %s)""",
            (item["Title"], item["Description"]),
        )
        self.connection.commit()