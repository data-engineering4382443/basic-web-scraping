This is a basic web scraping project using scrapy. The website used for this scraping project is provided by DataCamp.
The extracted data from the DataCamp courses website is saved to a .json file and to a locally hosted PostgreSQL database.

The spider is run by executing the command: "scrapy crawl course -O course.json" in the terminal